package digitounico.controller;

import java.security.PublicKey;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import digitounico.domain.Users;
import digitounico.encryption.EncryptDecryptsRSA;
import digitounico.entity.ClientCalcs;
import digitounico.entity.DigitoUnico;
import digitounico.repository.UsersRepository;


@RestController
public class DigitoUnicoController {
    @Autowired
    private UsersRepository usersRepository;
    
    @Autowired
    private DigitoUnico digitoUnico;
    
    @Autowired
    private EncryptDecryptsRSA encryptDecryptsRSA;

    @RequestMapping(value = "v1/user", method = RequestMethod.GET)
    public List<Users> Get() {
        return usersRepository.findAll();
    }

    @RequestMapping(value = "v1/new/user", method =  RequestMethod.POST)
    public Users create(@Valid @RequestBody Users user)
    {
        return usersRepository.save(user);
    }

    @RequestMapping(value = "v1/user/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Users> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Users newUser)
    {
        Users oldUser = usersRepository.findOne(id);
        if(oldUser != null){
        	Users user = oldUser;
            user.setNomeUsuario(newUser.getNomeUsuario());
            usersRepository.save(user);
            return new ResponseEntity<Users>(user, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "v1/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Users> Delete(@PathVariable(value = "id") long id)
    {
        Users user = usersRepository.findOne(id);
        if(user != null){
        	usersRepository.delete(user);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "v1/digitounico", method =  RequestMethod.POST)
    public int calcUserOptional(@Valid @RequestBody String value, int values)
    {
        return DigitoUnico.singleDigit(value, values);
    }

    @RequestMapping(value = "v1/history/digitounico/{id}", method =  RequestMethod.POST)
    public int userCalcs(@Valid @RequestBody String value, int values)
    {
        return DigitoUnico.singleDigit(value, values);
    }
    
    
    @RequestMapping(value = "v1/criptografia", method =  RequestMethod.POST)
    public byte[] PostEncrypt(@Valid @RequestBody String value, PublicKey chave)
    {
        return EncryptDecryptsRSA.criptografa(value, chave);
    }
   
    
}