package digitounico.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;

import digitounico.domain.Cache;

@Entity
public class DigitoUnico {
	
	public static boolean checkHasOneDigit(String value) {
		return value.length() == 1;
	}	

	public static int singleDigit (String value, int times){
		String concatValueBbyTimes = "";

		for(int i = 0; i < times; i++){
			concatValueBbyTimes += value;
		}

		return DigitoUnico.singleDigit(concatValueBbyTimes);
	}
	
	public static int singleDigit (String value){
		if(DigitoUnico.checkHasOneDigit(value)){
			return Integer.parseInt(value);
		} else {
			String[] explodedValue = value.split("");
			int total = 0;
			for(int i = 0; i < explodedValue.length; i++){
    			total += Integer.parseInt(explodedValue[i]);
    		}
    		
    		return total;
		}
	}
}