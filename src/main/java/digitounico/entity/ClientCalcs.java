package digitounico.entity;

import javax.persistence.Entity;

@Entity
public class ClientCalcs {
    private String digits;
	private int times;
    private int response;

	public ClientCalcs(String digits, int times, int response) {
		this.digits = digits;
		this.times = times;
		this.response = response;
	}
    
	public String getDigits() {
		return digits;
	}

	public void setDigits(String digits) {
		this.digits = digits;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public int getResponse() {
		return response;
	}

	public void setResponse(int response) {
		this.response = response;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((digits == null) ? 0 : digits.hashCode());
		result = prime * result + response;
		result = prime * result + times;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientCalcs other = (ClientCalcs) obj;
		if (digits == null) {
			if (other.digits != null)
				return false;
		} else if (!digits.equals(other.digits))
			return false;
		if (response != other.response)
			return false;
		if (times != other.times)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClientCalcs [digits=" + digits + ", times=" + times + ", response=" + response + "]";
	}

	
	
	
	
    
}