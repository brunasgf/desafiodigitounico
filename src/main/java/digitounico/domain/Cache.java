package digitounico.domain;

import java.util.HashMap;

import digitounico.entity.ClientCalcs;

public class Cache {
    private static HashMap<String, ClientCalcs> requests= new HashMap<String, ClientCalcs>();
    
    public static void addNewCache(ClientCalcs request) {
    	if(requests.size() >= 10) {    		
    		requests.values().forEach(x->{
        		String key = "" + request.getDigits() + request.getTimes();
        		requests.remove(key);
        		return;
        	});
    		
    		String key = "" + request.getDigits() + request.getTimes();
    		requests.put(key, request);
    	} else {
    		String key = "" + request.getDigits() + request.getTimes();
    		requests.put(key, request);
    	}
    }
    
    
    public static int getResponse(String digit, int times) {
    	String key = "" + digit + times;
    	ClientCalcs resp = requests.get(key);
    	if(resp != null) {
    		return resp.getResponse();
    	} else {
    		return -1;
    	}
    }
}