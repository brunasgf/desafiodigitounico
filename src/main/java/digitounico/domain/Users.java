package digitounico.domain;

import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import digitounico.entity.ClientCalcs;

public class Users {
	
  @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   	private Long id;

	public String email;
	
	public String nomeUsuario;
	
	public List <ClientCalcs> listaResultados;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	public List <ClientCalcs> getListaResultados() {
		return listaResultados;
	}

    public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listaResultados == null) ? 0 : listaResultados.hashCode());
		result = prime * result + ((nomeUsuario == null) ? 0 : nomeUsuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (listaResultados == null) {
			if (other.listaResultados != null)
				return false;
		} else if (!listaResultados.equals(other.listaResultados))
			return false;
		if (nomeUsuario == null) {
			if (other.nomeUsuario != null)
				return false;
		} else if (!nomeUsuario.equals(other.nomeUsuario))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Users [id=" + id + ", email=" + email + ", nomeUsuario=" + nomeUsuario + ", listaResultados="
				+ listaResultados + "]";
	}
	
	
	

}
