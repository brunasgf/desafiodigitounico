package digitounico.domain;

import org.junit.Test;
import org.meanbean.test.BeanTester;

import digitounico.entity.ClientCalcs;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import java.lang.reflect.Modifier;

public class PojoTest {
        
    private Class[] classes = {
            
            Users.class,
            ClientCalcs.class,
    };

    @Test
    public void equalsUsers() {
        EqualsVerifier.forClass(Users.class)
                .suppress(Warning.STRICT_INHERITANCE)
                .suppress(Warning.NONFINAL_FIELDS)
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();
    }

    @Test
    public void equalsClientCalcs() {
        EqualsVerifier.forClass(ClientCalcs.class)
                .suppress(Warning.STRICT_INHERITANCE)
                .suppress(Warning.NONFINAL_FIELDS)
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();
    }


        
    @Test
    public void getterAndSetterCorrectness() {
        BeanTester beanTester = new BeanTester();
        for (Class c : classes) {
            if (!Modifier.isAbstract(c.getModifiers())) {
                beanTester.testBean(c);
            }
        }
    }

}