package digitounico.entity;

import java.time.LocalDateTime;

import digitounico.domain.Cache;

public class DigitoUnicoTest {
	
	public static void main(String[] args) {
		
		System.out.println(LocalDateTime.now());
		int x = DigitoUnico.singleDigit("7", 100000);
		System.out.println(x);
		ClientCalcs request = new ClientCalcs("7", 100000, x);
		Cache.addNewCache(request);
		System.out.println(LocalDateTime.now());
		
		System.out.println(LocalDateTime.now());
		System.out.println(Cache.getResponse("7", 100000));
		System.out.println(LocalDateTime.now());
	}

}
